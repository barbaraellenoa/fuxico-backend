#!/usr/bin/python
# coding: utf-8
import json
import os
import shutil
import tempfile
import unittest
from copy import deepcopy
from io import BytesIO
from collections import namedtuple
from unittest.mock import patch, Mock
from subprocess import CompletedProcess, SubprocessError, CalledProcessError

from bottle import HTTPError, FileUpload
from boddle import boddle

from fuxico_backend import sementeira


class TestGetDiskUsage(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_no_main_dir_set(self):
        mk_os_environ = {"UPLOAD_FOLDER": ""}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                sementeira.get_disk_usage()
            self.assertEqual(str(exc_info.exception), "No upload directory set.")

    def test_success(self):
        _ntuple_diskusage = namedtuple('usage', 'total, used, free')
        st = os.statvfs(self.test_dir)
        expected = _ntuple_diskusage(
            st.f_blocks * st.f_frsize,
            (st.f_blocks - st.f_bfree) * st.f_frsize,
            st.f_bavail * st.f_frsize
        )
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            result = sementeira.get_disk_usage()
            self.assertEqual(result, expected)


class TestMakeInstall(unittest.TestCase):

    def setUp(self):
        sementeira_content = [
            "autonomia",
            "sexualidade",
            "dicas",
            "tenologias-digitais",
            "saude",
            "agroecologia",
        ]
        self.properties = {
            "netKey": "fuxico",
            "sementeira": sementeira_content,
            "skelentonKey": "fuxico",
            "storage": "sd",
            "sysKey": "fuxico",
        }

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_calls_os_chdir_to_change_to_install_dir(
        self, mk_subprocess_run, mk_os_chdir
    ):
        sementeira.make_install(self.properties)
        mk_os_chdir.assert_called_once_with("/opt/piratebox/bin")

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_calls_subprocess_run_with_storage_options(
        self, mk_subprocess_run, mk_os_chdir
    ):
        storage = {
            "sd": "SDCARD",
            "usb": "USB",
            "both": "BOTH",
        }
        command = [
            "SENHA_USUARIO=" + self.properties["sysKey"],
            "TIPO_ARMAZENAMENTO=SDCARD",
            "CONTENT_SEMENTEIRA=all",
            "SENHA_WIFI=" + self.properties["netKey"],
            "SENHA_ARQUIVOS=" + self.properties["skelentonKey"],
            "HABILITAR_RODA=true",
            "bash",
            "install.sh"
        ]
        for storage_type, value in storage.items():
            _properties = deepcopy(self.properties)
            _properties["storage"] = storage_type
            _command = deepcopy(command)
            _command.pop(1)
            _command.insert(1, "TIPO_ARMAZENAMENTO=" + storage[storage_type])
            with self.subTest(storage_type=storage_type):
                sementeira.make_install(_properties)
                mk_subprocess_run.assert_any_call(_command, check=True)

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_calls_subprocess_run_with_empty_sementeira_content(
        self, mk_subprocess_run, mk_os_chdir
    ):
        command = [
            "SENHA_USUARIO=" + self.properties["sysKey"],
            "TIPO_ARMAZENAMENTO=SDCARD",
            "CONTENT_SEMENTEIRA=none",
            "SENHA_WIFI=" + self.properties["netKey"],
            "SENHA_ARQUIVOS=" + self.properties["skelentonKey"],
            "HABILITAR_RODA=true",
            "bash",
            "install.sh"
        ]
        _properties = deepcopy(self.properties)
        _properties["sementeira"] = []
        sementeira.make_install(_properties)
        mk_subprocess_run.assert_called_once_with(command, check=True)

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_calls_subprocess_run_with_few_sementeira_content(
        self, mk_subprocess_run, mk_os_chdir
    ):
        command = [
            "SENHA_USUARIO=" + self.properties["sysKey"],
            "TIPO_ARMAZENAMENTO=SDCARD",
            "CONTENT_SEMENTEIRA=autonomia,dicas,saude",
            "SENHA_WIFI=" + self.properties["netKey"],
            "SENHA_ARQUIVOS=" + self.properties["skelentonKey"],
            "HABILITAR_RODA=true",
            "bash",
            "install.sh"
        ]
        _properties = deepcopy(self.properties)
        _properties["sementeira"] = ["autonomia", "dicas", "saude"]
        sementeira.make_install(_properties)
        mk_subprocess_run.assert_called_once_with(command, check=True)

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_calls_subprocess_run_without_optional_properties(
        self, mk_subprocess_run, mk_os_chdir
    ):
        command = [
            "SENHA_USUARIO=" + self.properties["sysKey"],
            "TIPO_ARMAZENAMENTO=SDCARD",
            "CONTENT_SEMENTEIRA=all",
            "SENHA_WIFI=" + self.properties["netKey"],
            "SENHA_ARQUIVOS=" + self.properties["skelentonKey"],
            "HABILITAR_RODA=true",
            "bash",
            "install.sh"
        ]
        for var, _optional_param in (
                ("SENHA_WIFI=", "netKey"), ("SENHA_ARQUIVOS=", "skelentonKey")):
            _properties = deepcopy(self.properties)
            _command = deepcopy(command)
            _command.remove(var + _properties[_optional_param])
            del _properties[_optional_param]
            with self.subTest(_optional_param=_optional_param):
                sementeira.make_install(_properties)
                mk_subprocess_run.assert_any_call(_command, check=True)

    @patch("fuxico_backend.sementeira.os.chdir")
    @patch("fuxico_backend.sementeira.subprocess.run")
    def test_install_ok(self, mk_subprocess_run, mk_os_chdir):
        command = [
            "SENHA_USUARIO=" + self.properties["sysKey"],
            "TIPO_ARMAZENAMENTO=SDCARD",
            "CONTENT_SEMENTEIRA=all"
            "SENHA_WIFI=" + self.properties["netKey"],
            "SENHA_ARQUIVOS=" + self.properties["skelentonKey"],
            "HABILITAR_RODA=true",
            "bash",
            "install.sh"
        ]
        mk_subprocess_run.return_value = CompletedProcess(args=command, returncode=0)
        sementeira.make_install(self.properties)


class TestRenameFile(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_no_main_dir_set(self):
        mk_os_environ = {"UPLOAD_FOLDER": ""}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                sementeira.rename_file(None, None)
            self.assertEqual(str(exc_info.exception), "No upload directory set.")

    def test_file_does_not_exist(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                result = sementeira.rename_file("test.png", "new_test.png")
            self.assertEqual(str(exc_info.exception), "No such file or directory.")

    def test_success(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with open(os.path.join(self.test_dir, "test.png"), "w+b") as fp:
                result = sementeira.rename_file("test.png", "new_test.png")
                self.assertTrue(
                    os.path.exists(os.path.join(self.test_dir, "new_test.png"))
                )
                self.assertFalse(
                    os.path.exists(os.path.join(self.test_dir, "test.png"))
                )


class TestGetSharedFilesContent(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_no_main_dir_set(self):
        mk_os_environ = {"UPLOAD_FOLDER": ""}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                sementeira.get_shared_files_content(None)
            self.assertEqual(str(exc_info.exception), "No upload directory set.")

    def test_access_main_dir(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        expected = ["Folder_{}.png".format(i) for i in range(1, 4)]
        files = [
            open(os.path.join(self.test_dir, filename), "w+b")
            for filename in expected
        ]
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            result = sementeira.get_shared_files_content(None)
            self.assertEqual(result, expected)
        [file.close() for file in files]

    def test_filepath_check_if_it_exists(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                result = sementeira.get_shared_files_content("test.png")
            self.assertEqual(str(exc_info.exception), "No such file or directory.")

    def test_returns_file(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with open(os.path.join(self.test_dir, "test.png"), "w+b") as fp:
                fp.write(b"test.png file")
                result = sementeira.get_shared_files_content("test.png")
                self.assertEqual(result.body.read(), fp.read())
                self.assertEqual(result.body.name, fp.name)

    def test_get_shared_files_content_returns_dir(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            os.mkdir(os.path.join(self.test_dir, "Test"))
            with open(os.path.join(self.test_dir, "Test", "test.png"), "w+b") as fp:
                fp.write(b"test.png file")
                result = sementeira.get_shared_files_content("Test")
                self.assertEqual(result, ["test.png"])


class TestSaveFileContent(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)

    def test_no_main_dir_set(self):
        mk_os_environ = {"UPLOAD_FOLDER": ""}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            with self.assertRaises(FileNotFoundError) as exc_info:
                sementeira.save_file_content(Mock())
            self.assertEqual(str(exc_info.exception), "No upload directory set.")

    def test_saves_file(self):
        mk_os_environ = {"UPLOADFOLDER": self.test_dir}
        with patch.dict('fuxico_backend.sementeira.os.environ', mk_os_environ):
            fp = FileUpload(BytesIO(b"test.png file"), "file-upload", "test.png")
            sementeira.save_file_content(fp)
            self.assertTrue(os.path.exists(os.path.join(self.test_dir, "test.png")))


class TestSharedFiles(unittest.TestCase):
    @patch.object(sementeira, "get_shared_files_content", return_value="")
    def test_calls_get_shared_files_content(self, mk_get_shared_files_content):
        sementeira.shared_files("filepath")
        mk_get_shared_files_content.assert_called_once_with("filepath")

    @patch.object(sementeira, "get_shared_files_content")
    def test_path_not_found(self, mk_get_shared_files_content):
        mk_get_shared_files_content.side_effect = FileNotFoundError("File not found.")
        with self.assertRaises(HTTPError) as exc_info:
            sementeira.shared_files()
        self.assertEqual(
            exc_info.exception.body,
            'Could not get shared file "/Shared/": File not found.'
        )

    @patch.object(sementeira, "get_shared_files_content")
    def test_returns_content_on_success(self, mk_get_shared_files_content):
        expected = ["Folder_{}".format(i) for i in range(1, 11)]
        mk_get_shared_files_content.return_value = expected
        result = sementeira.shared_files("dir_test")
        self.assertEqual(result, json.dumps(expected))


class TestRename(unittest.TestCase):
    @patch.object(sementeira, "rename_file")
    def test_returns_error_if_no_request_new_filename(self, mk_rename_file):
        with boddle(params={}):
            with self.assertRaises(HTTPError) as exc_info:
                sementeira.rename("filepath")
            self.assertEqual(
                exc_info.exception.body,
                'new_filename field is mandatory.'
            )

    @patch.object(sementeira, "rename_file")
    def test_calls_rename_file(self, mk_rename_file):
        with boddle(params={"new_filename": "new"}):
            sementeira.rename("filepath")
            mk_rename_file.assert_called_once_with("filepath", "new")

    @patch.object(sementeira, "rename_file")
    def test_file_not_found(self, mk_rename_file):
        mk_rename_file.side_effect = FileNotFoundError("File not found.")
        with boddle(params={"new_filename": "new"}):
            with self.assertRaises(HTTPError) as exc_info:
                sementeira.rename("filepath")
            self.assertEqual(
                exc_info.exception.body,
                'Could not rename file "filepath": File not found.'
            )

    @patch.object(sementeira, "rename_file")
    def test_returns_no_content_on_success(self, mk_rename_file):
        with boddle(params={"new_filename": "new"}):
            result = sementeira.rename("filepath")
            self.assertEqual(result.status_code, 204)


class TestDiskUsage(unittest.TestCase):
    def setUp(self):
        _ntuple_diskusage = namedtuple('usage', 'total, used, free')
        self.disk_info = _ntuple_diskusage(
            8589934592,
            4294967296,
            4294967296
        )

    @patch.object(sementeira, "get_disk_usage")
    def test_calls_get_disk_usage(self, mk_get_disk_usage):
        mk_get_disk_usage.return_value = self.disk_info
        sementeira.disk_usage()
        mk_get_disk_usage.assert_called_once()

    @patch.object(sementeira, "get_disk_usage")
    def test_path_not_found(self, mk_get_disk_usage):
        mk_get_disk_usage.side_effect = FileNotFoundError("No upload directory set.")
        with self.assertRaises(HTTPError) as exc_info:
            result = sementeira.disk_usage()
        self.assertEqual(
            exc_info.exception.body,
            "Could not get disk usage: No upload directory set."
        )

    @patch.object(sementeira, "get_disk_usage")
    def test_returns_disk_info(self, mk_get_disk_usage):
        _ntuple_diskusage = namedtuple('usage', 'total, used, free')
        expected = {
            "total": 8589934592,
            "used": 4294967296,
            "free": 4294967296,
        }
        mk_get_disk_usage.return_value = self.disk_info
        result = sementeira.disk_usage()
        self.assertEqual(result, json.dumps(expected))


if __name__ == "__main__":
    unittest.main()
