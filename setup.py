#!/usr/bin/env python3
import os, setuptools

setup_path = os.path.dirname(__file__)

with open(os.path.join(setup_path, "README.md")) as readme:
    long_description = readme.read()

setuptools.setup(
    name="fuxico_backend",
    version="0.1",
    author="MariaLab",
    author_email="fuxico@protonmail.com",
    description="Backend que manipula os arquivos da Fuxico."
    "Lista, lê, salva e deleta arquivos.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="2-clause BSD",
    packages=setuptools.find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests", "docs"]
    ),
    include_package_data=False,
    python_requires=">=3.6",
    install_requires=[
        "bottle~=0.12",
        "paste~=3.1.0",
    ],
    tests_require=[
        "WebTest~=2.0",
        "boddle~=0.2",
    ],
    test_suite="tests",
    classifiers=(
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Other Environment",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Operating System :: OS Independent",
    ),
)
